export enum ContentTypes {
  JSON = 'application/json',
  JSON_UTF = 'charset=utf-8; application/json'
}

export enum ExponentialBackoff {
  INITIAL_EXPO_BACKOFF = 10,
  DELAY_EXPO_BACKOFF = 5
}

export enum RetryConfiguration {
  INITIAL_RETRY = 0,
  MAX_RETRIES = 3
}

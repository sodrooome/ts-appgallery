import { InterfacesFilePath } from '../interfaces/interfacesFilePath'
import { promises as fs } from 'fs'
import { HttpLogger, Singleton } from './logger'
import path from 'path'

const logger: HttpLogger = Singleton.getInstance()

export async function findFilePath ({
  pathAppDirectory,
  fileExt
}: InterfacesFilePath): Promise<{}> {
  // TODO: need implement try-catch block
  const filePaths: {} = await Promise.resolve(
    searchFileApp(pathAppDirectory, fileExt)
  )
  return filePaths
}

export async function searchFileApp (
  appName: string,
  fileExt?: string
): Promise<{}> {
  const rootFolders: string[] = []
  const typeFileExt: string[] = [
    'apk',
    'rpk',
    'pdf',
    'jpg',
    'jpeg',
    'png',
    'bmp',
    'mp4',
    'mov',
    'aab'
  ]
  let paths = {}

  for (var indexFolder in rootFolders) {
    const appNamePath: string = `${appName}/${indexFolder}`
    const appNameStat = await fs.lstat(appNamePath)
    const maxFileSize: number = 4096

    if (appNameStat.isFile()) {
      const fileExtension: string = indexFolder.split('.').pop() || ''

      // not sure this is a correct validation
      // for check fileExtension in array, it
      // could be using includes method instead
      // or using hasOwnProperty
      if (typeFileExt.indexOf(fileExtension)) {
        logger.log('Found file app name with matches the extension')
        continue
      } else {
        throw new Error('Not supported for that file extension')
      }
    }

    if (appNameStat.size >= maxFileSize) {
      throw new Error('Initial size is bigger with maximum file size')
    }

    const fullPathFileName = path.resolve(appNamePath)
    const fileContent = await fs.readFile(fullPathFileName)

    paths = [fileContent, fullPathFileName]
  }

  return paths
}

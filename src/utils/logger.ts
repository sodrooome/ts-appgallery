// simple class that initiate logger
// stream handler accross different module
export class HttpLogger {
  logInfo = []

  constructor () {
    this.logInfo = []
  }

  log (message: string) {
    const datetimeFormat: string = new Date().toISOString()
    // can't use push method only, need specify the apply()
    this.logInfo.push.apply({ message, datetimeFormat })
    console.log(`[HTTP Logger] :: ${datetimeFormat} -  ${message}`)
  }
}

// Singleton class for HttpLogger that ensure
// only 1 instance that always be instantiated
export class Singleton {
  static instance: HttpLogger
  constructor () {}
  static getInstance (): HttpLogger {
    if (!Singleton.instance) {
      Singleton.instance = new HttpLogger()
    }
    return Singleton.instance
  }
}

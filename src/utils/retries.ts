import { ExponentialBackoff, RetryConfiguration } from '../constants/enums'
import axios from 'axios'

function setExponentialBackoff (retry: number): Promise<void> {
  retry = Math.min(ExponentialBackoff.INITIAL_EXPO_BACKOFF, retry)
  const initialDelay: number =
    ExponentialBackoff.DELAY_EXPO_BACKOFF * Math.pow(2, retry)
  return new Promise(resolve => setTimeout(() => resolve(), initialDelay))
}

function retryWrapper (axios, attemptRetry: number, options?: any) {
  const retryStatusCodes = options.retryStatusCode
  const allowedRetryStatus = [429, 503, 501, 500]
  let isSuccess: boolean = false // just for marking whether retry is success or not

  if (attemptRetry >= RetryConfiguration.MAX_RETRIES) {
    throw new Error('Max attempt retries only up to 3 retries')
  }

  if (!allowedRetryStatus.includes(retryStatusCodes)) {
    throw new Error(
      'Attempt retry status code only raised for: 429, 503, 501 & 500'
    )
  }

  axios.interceptors.response.use(null, (error: { config: any }) => {
    const config = error.config

    if (
      RetryConfiguration.INITIAL_RETRY <= attemptRetry &&
      retryStatusCodes in allowedRetryStatus
    ) {
      RetryConfiguration.INITIAL_RETRY
      isSuccess = true
      return new Promise(resolve => {
        resolve(config(axios))
      })
    }
    isSuccess = false
    return Promise.reject(error)
  })
}

const HttpRetries = {
  setExponentialBackoff,
  retryWrapper
}

export default HttpRetries

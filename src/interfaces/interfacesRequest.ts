export interface InterfacesRequest {
  clientId: string
  clientSecret: string
  appId: string
  fileExt: string
}

export interface InterfacesResponse {
  statusCode: number
  data?: any
  headers?: any
  additionalResponse?: any
}

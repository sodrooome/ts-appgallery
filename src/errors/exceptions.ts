export class HttpClientErrors extends Error {
  // code in here its represent
  // Huawi AppGallery code error
  code: number
  headers: any
  data: any
  message: string = ''
  statusCode: number

  constructor (
    code: number,
    message: string,
    headers: any,
    data: any,
    statusCode: number
  ) {
    // needs derived super constructor
    super(message)
    ;(this.code = code),
      (this.headers = headers),
      (this.data = data),
      (this.statusCode = statusCode)
  }
}

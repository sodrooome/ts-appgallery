import { InterfacesResponse } from '../interfaces/interfacesResponse'

export class HttpResponse implements InterfacesResponse {
  statusCode: number
  data?: any
  headers?: any
  additionalInformation?: any

  constructor (
    statusCode: number,
    data?: any,
    headers?: any,
    additionalInformation?: any
  ) {
    this.statusCode = statusCode
    this.data = data || {}
    this.headers = headers || {}
    this.additionalInformation = additionalInformation || {}
  }
}

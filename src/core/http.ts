import axios, { AxiosInstance, AxiosResponse } from 'axios'
import { promises as fs } from 'fs'
import { join } from 'path'
// import database from 'mime-db'
import { ContentTypes } from '../constants/enums'
import { InterfacesFilePath } from '../interfaces/interfacesFilePath'
import { InterfacesRequest } from '../interfaces/interfacesRequest'
import { findFilePath } from '../utils/findFilePath'
import { HttpLogger, Singleton } from '../utils/logger'
import HttpRetries from '../utils/retries'

const BASE_URL: string = 'https://connect-api.cloud.huawei.com/api'

// should i put this instance in index.ts?
// or just calling in here? not sure..
const logger: HttpLogger = Singleton.getInstance()

// base instances for HttpClient
// that wrapped around AxiosInstance,
// any additional headers or request body
// will goes through here
const httpClient = (
  requestUrl: string,
  data?: any,
  headers?: any,
  maxContentLength?: number,
  maxBodyLength?: number
): AxiosInstance =>
  axios.create({
    baseURL: requestUrl,
    timeout: 120,
    data: data,
    headers: headers,
    maxContentLength: maxContentLength,
    maxBodyLength: maxBodyLength
  })

export async function getToken (
  clientId: string,
  clientSecret: string
): Promise<AxiosResponse> {
  const data = JSON.stringify({
    grant_type: 'client_credentials',
    client_id: clientId,
    client_secret: clientSecret
  })

  HttpRetries.retryWrapper(axios, 2, { retryStatusCode: 429 })

  const response = <AxiosResponse>(
    await httpClient(BASE_URL, data, ContentTypes.JSON).get('/oauth2/v1/token')
  )

  // TODO: implement try-catch exception
  // for getting HTTP response and also
  // formatted with custom exception and
  // custom HTTP response based on Huawei
  // AppGallery status codes and message
  logger.log('Successfully obtain Huawei App token')
  return response['token']
}

// https://stackoverflow.com/questions/43051291/attach-authorization-header-for-all-axios-requests
// This method is used to attach all access tokens
// into all requests so it doesn't take to always
// instantiated the token. Actually, there are 2 solutions
// to solve this issue: using class or using this instead.
// This is eventually have similar function like
// __call__ method in python
export async function fetchAccessToken (
  clientId: string,
  clientSecret: string
) {
  const data = JSON.stringify({
    grant_type: 'client_credentials',
    client_id: clientId,
    client_secret: clientSecret
  })

  let fullPathUrl = BASE_URL + '/oauth2/v1/token'
  const instanceToken = httpClient(fullPathUrl, data, ContentTypes.JSON)

  instanceToken.interceptors.request.use(function (config) {
    const savedToken = localStorage.getItem('access_token')

    if (savedToken == undefined || savedToken == null) {
      config.headers!.Authorization = 'Bearer' + savedToken
      // always delete existing Authorization if exist
    } else if (savedToken != undefined || savedToken == null) {
      delete axios.defaults.headers.common['Authorization']
    }
    return config
  })

  return instanceToken
}

export async function getUploadUrl (
  appId: InterfacesRequest,
  fileExt: InterfacesRequest,
  clientId: InterfacesRequest,
  token: string
): Promise<AxiosResponse> {
  const headers = {
    client_id: clientId,
    Authorization: 'Bearer' + token
  }

  const response = <AxiosResponse>(
    await httpClient(BASE_URL, headers).get(
      `/publish/v2/upload-url?appId=${appId}&suffix=${fileExt}`
    )
  )

  logger.log('Successfully obtain Huawei App Information')
  return response
}

export async function submitApp (
  appId: string,
  clientId: string,
  token: string
): Promise<AxiosResponse> {
  const headers = {
    clien_id: clientId,
    Authorization: 'Bearer' + token,
    'Content-Type': ContentTypes.JSON
  }

  const response = <AxiosResponse>(
    await httpClient(BASE_URL, headers).post(
      `/publish/v2/app-submit?appId=${appId}`
    )
  )

  logger.log('Successfully submitted app into App Gallery')
  return response
}

export async function uploadFileApp (
  authCode: string,
  uploadUrl: string,
  filePath: string
): Promise<AxiosResponse> {
  // TODO: got stuck about assigne error for type Promise
  // let findFile = findFilePath({ pathAppDirectory: filePath, fileExt: 'apk' }).then((value => {}))
  // temporary using fs modules from finding app file
  // in directory / root dir of current path
  const typeFileExt: string[] = [
    '.apk',
    '.rpk',
    '.pdf',
    '.jpg',
    '.jpeg',
    '.png',
    '.bmp',
    '.mp4',
    '.mov',
    '.aab'
  ]

  if (!typeFileExt.some(value => filePath.endsWith(value))) {
    throw new Error('Currently not supported for that extension')
  } else {
    logger.log('File type extension is matches')
  }

  let findFile = await fs.readFile(join(__dirname, filePath), 'utf-8')
  logger.log('Successfully get the app file')

  let data: string[] = []
  data.push('authCode', authCode)
  data.push('fileCount', '1')
  data.push('file', findFile)

  const headers = {
    accept: ContentTypes.JSON
  }

  var maxBodyLength: number = 100000000
  var maxContentLength: number = 100000000

  const response = <AxiosResponse>(
    await httpClient(BASE_URL, headers, maxContentLength, maxBodyLength).post(
      uploadUrl,
      data
    )
  )

  return response
}

export async function updateAppInformation (
  fileDestUrl: string,
  size: number,
  appId: string,
  clientId: string,
  token: string,
  fileExt: string,
  fileName: string
): Promise<AxiosResponse> {
  const data = JSON.stringify({
    fileType: '5',
    files: [
      { fileName: `${fileName}.${fileExt}`, fileDestUrl: fileDestUrl, size }
    ]
  })

  const headers = {
    client_id: clientId,
    Authorization: 'Bearer' + token,
    'Content-Type': ContentTypes.JSON
  }

  const response = <AxiosResponse>(
    await httpClient(BASE_URL, headers, data).put(
      `/publish/v2/app-file-info?appId=${appId}`
    )
  )

  return response
}

export type ExpirationStatus =
  | 'Token is expired or invalid'
  | 'Token is refreshed'
  | 'Token is active'

export function checkTokenExpiration (
  clientId: string,
  clientSecret: string
): ExpirationStatus {
  const startDate = Date.now()

  const getExpiresIn = getToken(clientId, clientSecret)

  if (
    getExpiresIn['access_token'] !== undefined &&
    getExpiresIn['expires_in'] > startDate
  ) {
    return 'Token is active'
  } else {
    return 'Token is expired or invalid'
  }
}
